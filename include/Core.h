#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED
#include"Constants.h"
#include"Vector3.h"
#include"Particle.h"
#include"Rnd.h"
#include<cmath>

inline double F_rot_norm(const double& en){return sqrt(en / 0.5) * exp(0.5 - en); } //normalized distribution function for rotational energy initialization process, see
                                                                                    //Pfeiffer et al. "Direct simulation Monte Carlo modeling of relaxation processes in polyatomic gases"
inline double F_vib_norm(const double& en,  const double& viscInd)
{
    return pow(1.0 - en, 1.5 - viscInd);
   // return pow(e_c - e_v ,1.5 - viscInd) / pow(e_c, 1.5 - viscInd);

}



inline double e_v_aho(const double& o_v, const double& o_d, const double& eta, const double& n)
{
    return kBoltzmann * o_v * (n) * (1.0 - (eta * o_v * (n) / (4.0 * o_d)) );
}





inline double getNmaxAHO(const double& o_v, const double& o_d, const double& eta) {return floor(2.0 * o_d / o_v * (1.0 - sqrt(1.0 - eta)) / eta);}

inline double Q_t(const double& o_v, const double& temp, const double& o_d, const double& eta)
{
    double res = 0.0;
    int n_max = getNmaxAHO(o_v,o_d,eta);
    for(int n = 0; n <= n_max; ++n)
    {
        res += exp(-1.0 * o_v / temp * (n) * (1.0 - eta * o_v * (n) / (4.0 * o_d) ));
    }
    return res;
}

inline double f_v_aho(const double& o_v, const double& o_d, const double& temp, const double& eta, const int& n)
{
    return exp(- 1.0 * e_v_aho(o_v, o_d,eta,n) / (kBoltzmann * temp)) / Q_t(o_v, temp, o_d, eta);
}

double generateVibEnAHO(const double& o_v, const double& o_d, const double& temp, const double& eta);

inline double generateRotEn(const double& rot_dof,const double& rot_temp);

inline double generateVibEn(const double& char_temp, const double& vib_temp);

inline double massReduced(const double& m1, const double& m2) {return m1 == m2 ? m1 / 2.0 : m1 * m2 / (m1 + m2);}    //return reduced mass
inline double valueAvg(const double& v1, const double& v2) {return v1 == v2 ? v1 : (v1 + v2) / 2.0;}     //return average of two values

inline double Xi_t(const double& viscIndx){return 5.0 - (2.0 * viscIndx);}  //translational degree of freedom
inline double Xi_v(const double& charTemp, const double& temp)
{
    if(charTemp == 0)
        return 0.0;
    return 2.0 * charTemp / temp /(exp(charTemp / temp) - 1.0);
}

inline double Z_r(const double& intDof, const double& viscIndx, const double& temp)
{
    //return Z_r_c *  Xi_t(viscIndx) / (Xi_t(viscIndx) + intDof);
    return Z_r_inf * Xi_t(viscIndx) / (Xi_t(viscIndx) + intDof) / (1.0 + (pow(M_PI, 1.5) / 2.0) * sqrt(T_inf / temp) + (M_PI + M_PI * M_PI / 4.0) * (T_inf / temp) );
}

inline double Z_v(const double& charTemp, const double& viscIndx, const double& temp, const double& massRed, const double& diamAvr)
{

    double a = 1.16e-3 * sqrt(massRed / 0.16603e-26) * pow(charTemp, 4.0 / 3.0);
    double b = - 1.74e-5 * pow(massRed / 0.16603e-26, 0.75) * pow(charTemp, 4.0 / 3.0) - 18.42;
    double tau_mw =  101325.0 * exp(a * pow(temp, - 1.0 / 3.0) + b) / (kBoltzmann * number_dens * temp);   //millikan - white

    double c_avr = 2.0 * sqrt(kBoltzmann * temp / massRed / M_PI);
    double tau_p = 1.0 / (number_dens * c_avr * 1e-20);

    double freq = 2.0 * pow(diamAvr, 2.0) * number_dens * pow(temp / T_ref, 1.0 - viscIndx) * sqrt(2.0 * M_PI * kBoltzmann * T_ref / massRed);


    double Z_v_0 = (tau_mw + tau_p) * freq;
    return Z_v_0 * Xi_t(viscIndx) / (Xi_t(viscIndx) + 0.5 * pow(Xi_v(charTemp, temp), 2.0) * exp(charTemp / temp));
    //return Z_v_c * Xi_t(viscIndx) / (Xi_t(viscIndx) + 0.5 * pow(Xi_v(charTemp, temp), 2.0) * exp(charTemp / temp));
}

inline double delta1(const int a, const int b) {return a == b ? 1.0 : 0.0;}     //delta functions for
inline double delta2(const int a, const int b) {return a == b ? 0.5 : 1.0;}     //the MF Scheme
inline double delta3(const int a, const int b) {return a == b ? 2.0 : 1.0;}     //for TCE dissociation probability
inline double delta4(const int a, const int b) {return a == b ? 1.0 : 2.0;}

inline double calcBeta(const double& mass, const double& t_tr)  //means the value of (1 / most probable speed)
{
    return sqrt(mass / (2.0 * kBoltzmann * t_tr));
}

inline double generateSigmaCr(const double& mass, const double& diam, const double& temp)
{
    return M_PI * pow(diam, 2.0) / calcBeta(mass,temp) * 3.0;    //max speed could be raised to (3 * most probable speed)
}


Vector3 generateVel(const double& mass, const double& t_tr,const Vector3&);
void init_particles(vector<Particle>& ,const int, const string ,const int ,const Temperature& , const Vector3& );

inline double calcSigmaVHS(const double& T_ref, const double& diam, const double& mass, const double& vel_rel, const double& visc_ind)  //calculate total collision cross-section according to Variable Hard Sphere model
{
    return M_PI * pow(diam, 2.0) * (pow(2.0 * kBoltzmann * T_ref / (mass * vel_rel * vel_rel), (visc_ind - 0.5)) / tgamma(2.5 - visc_ind)); //see (4.63) in Bird "Molecular gas dynamics" book
}

inline double Y_r(const double& dof) { return dof / 2.0; }    //function return the half of internal degree of freedom value
inline double Y_c(const double& vi) { return 5.0 / 2.0 - vi; }    //function return the half of translational degree of freedom value
double new_e_int(const double& dof, const double& viscInd, const double& e_coll); //return new value of internal energy after collision
#endif // CORE_H_INCLUDED
