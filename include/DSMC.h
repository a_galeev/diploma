#include<vector>
#include<string>
#include<stdlib.h>
#include<cmath>
#include"Rnd.h"
#include"Constants.h"
#include"Vector3.h"
#include"Particle.h"
#include"Core.h"
#include"Output.h"

using namespace std;

class DSMC
{
    int sz; //number of species in simulation
    int sp1, sp2;   //id for selected species
    double f_num;   //number of gas molecules represented by a modeling molecule [N_real / N_comp]
    double mf;  //total majorant collision frequency (for MF Scheme)
    int nc = 0; //number of collisions
    int inelastic = 0;  //number of inelastic collisions

    Vector3 cr_vec; //vector3 of relative velocity of particles
    double rnd; // for random
    double massRed, diamAvr, viscIndAvr, rotDofAvr, cr, sigma_cr;
    int dissCounter = 0;
    int ch_c = 0;

    vector<vector<double> > sigma_cr_arr;   //array 2d for (sigma * relative_speed) values
    vector<vector<double> > mf_arr; //  majorant frequency values array
    vector<vector<int> > nc_arr;    //number of collisions array
    vector< vector<double> > w_coll_theory;   //frequency of collision array
    vector< vector<double> > w_coll_calc;

    vector<double> sp_w;    //array of species probability values
    vector< pair<int, int> > pair_w;    //array of species pairs for previous array of species probability values
    vector<double> sum_w;   //transformed array of species probabilities with values in [0,1] range

    void init_sigma_cr_arr (vector<string>&, const double&);    //for the sigma_cr array initialization
    void init_mf_arr(vector<int>&);    //for the majorant frequency array initialization
    void init_sp_sel(); //for species selection probability initialization

    inline double generate_dTau(double& mf){return -1.0 * log(Rand.getStdDistr_trunc()) / mf;}



    inline int getRndParticle(vector<int> &num_of_part, int& id);    //get random particle  by selected specie id

    inline void updateSigmaCr(const double&);
    inline void collision(Particle&, const int, Particle&, const int,vector<Particle>&,  vector<int>&, Temperature&  );
    Temperature calcTemp(vector<Particle>&, vector<int>& );
    void calibrateTrTemp(vector<Particle>& _p, vector<int> &num_of_part,Temperature& temp);
    void init_Wcoll_theory(vector<string>& , vector<int> &, double);
public:
    DSMC(const int&, vector<string>& , vector<int> &, Temperature&);   //main procedure??
};


