#ifndef CONSTANTS_H
#define CONSTANTS_H


#include<cmath>
#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif
#include<limits>
#include<string>
#include<fstream>
#include<vector>
#include<iostream>
#include<map>
#include<utility>



using namespace std;

static const double kBoltzmann = 1.38065e-23;  //Boltzmann constant [Joule/K]
static const double NAvogadro = 6.022140857e23;    //Avogadro number [mole^-1]
static const double v_c = 1; //cell volume[m^-3]
static const double number_dens = 1e21;    //numerical density [m^-3]
static const double delta_t = 1e-6;    //such that N_coll ~ 0.1 * N_particles
static const double T_ref = 273.0;  //reference temperature value according to Bird's book
static const double Z_r_c = 5.0;   // (1 / probability) of rotational inelastic collision
static const double Z_v_c = 50.0;   //(9.1 / pow(300.0, 0.77)) * exp(220.0 / pow(300.0, -1.0 / 3.0));
static const double Z_r_inf = 23.0;/*14.4;18.1;*/
static const double T_inf = /*90.0;*/91.5;
static const double eta = 0.98;
static const double Ad = 1.83e-10;//4.093e-11;//1.83e-10;//2.56e-10;
static const double Bd = -1.05;//-1.0;
static const double Ad_b = 0.4;
static const double Bd_b = 2.0;
static const double f_b = 3.0;



class Chem_el
{
    double _mass;   //mass
    double _diameter;   //diameter
    double _visc_indx;  //viscosity index
    double _rotDof; //rotational degree of freedom
    double _vibCharTemp;   //vibrational characteristic temperature
    double _diss_Char_temp; //dissociation characteristic temperature

public:
    Chem_el(){}
    Chem_el(double mass,double diam,double visc_ind, double rotDof, double vibCharTemp, double diss_Char_temp)
        :_mass{mass},_diameter{diam},_visc_indx{visc_ind}, _rotDof{rotDof}, _vibCharTemp{vibCharTemp}, _diss_Char_temp{diss_Char_temp} {}
    inline double getMass() const {return _mass;}
    inline double getDiam() const {return _diameter;}
    inline double getViscInd() const {return _visc_indx;}
    inline double getRotDof() const {return _rotDof;}
    inline double getVibCharTemp() const {return _vibCharTemp;}
    inline double getDissCharTemp() const {return _diss_Char_temp;}

};

map<string, Chem_el> initChemEl();


const  map<string, Chem_el> elements = initChemEl();



class Temperature
{
    double _t_tr, _t_rot, _t_vib, _t_ov;   //translational, rotational, vibrational and overall temperatures
public:
    Temperature()
        :_t_tr{0},_t_rot{0}, _t_vib{0}, _t_ov{0} {}
    Temperature(double t_tr, double t_rot, double t_vib, double t_ov)
            :_t_tr{t_tr},_t_rot{t_rot}, _t_vib{t_vib}, _t_ov{t_ov} {}
    inline double getT_tr() const{return _t_tr;}
    inline void setT_tr(double temp) {_t_tr = temp;}
    inline double getT_rot() const {return _t_rot;}
    inline double getT_vib() const {return _t_vib;}
    inline double getT_ov() const {return _t_ov;}
    inline void setTemp(double t_tr, double t_rot, double t_vib, double t_ov)
    {
        _t_tr=t_tr;
        _t_rot=t_rot;
        _t_vib = t_vib;
        _t_ov=t_ov;
    }
    friend ostream& operator <<(ostream& os, const Temperature& temp)
    {
        return os<<temp.getT_tr()<<';'<<temp.getT_rot()<<';'<<temp.getT_vib()<<';'<<temp.getT_ov()<<';';
    }

};

#endif // CONSTANTS_H
