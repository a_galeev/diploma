#ifndef PARTICLE_H
#define PARTICLE_H
#include"Vector3.h"


class Particle
{
    Vector3 _vel;   //velocity
    int _id;    //identificator number
    double _mass;   //mass
    double _diameter;   //diameter
    double _visc_indx;  //viscosity index
    double _rotDof; //rotational degree of freedom
    double _rotEnergy;  //rotational energy
    double _charTemp;   //characteristic vibr temp
    double _dissTemp;   //characteristic dissociation temperature
    double _vibEnergy;  //vibrational energy
    //inline double _trDof(){return 5.0 - 2.0 * _visc_indx;}    //translational degree of freedom
public:
    Particle() = default;
    Particle(Vector3 vel, int id, double mass, double diam, double visc_ind, double rotDof, double rotEn, double charTemp, double dissTemp, double vibEn)
        :_vel{vel}, _id{id}, _mass{mass}, _diameter{diam}, _visc_indx{visc_ind}, _rotDof{rotDof}, _rotEnergy{rotEn}, _charTemp{charTemp}, _dissTemp{dissTemp}, _vibEnergy{vibEn} {}
    inline Vector3 getVel(){return _vel;}
    inline int getId(){return _id;}
    inline double getMass(){return _mass;}
    inline double getDiam(){return _diameter;}
    inline double getViscInd(){return _visc_indx;}
    inline double getRotDof(){return _rotDof;}
    inline double getRotEn() {return _rotEnergy;}
    inline double getCharTemp() {return _charTemp; }
    inline double getDissTemp() {return _dissTemp; }
    inline double getVibEn() {return _vibEnergy;}
    inline void setVel(const Vector3& vel){_vel = vel;}
    inline void setRotEn(const double& rotEn){_rotEnergy = rotEn;}
    inline void setVibEn(const double& vibEn){_vibEnergy = vibEn;}
};

#endif // PARTICLE_H
