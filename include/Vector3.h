#ifndef VECTOR3_H
#define VECTOR3_H
#include<cmath>
#include<iostream>

using namespace std;

class Vector3 //class of 3 variables for velocities, coordinates etc.
{
    double _x,_y,_z;
    friend Vector3 operator + (const Vector3& v1, const Vector3& v2)
    {
        return Vector3{v1._x + v2._x,v1._y+v2._y,v1._z+v2._z };
    }
    friend Vector3 operator * (const Vector3& v1, const Vector3& v2)
    {
        return Vector3{v1._x * v2._x,v1._y*v2._y,v1._z*v2._z };
    }
    friend Vector3 operator - (const Vector3& v1, const Vector3& v2)
    {
        return Vector3{v1._x - v2._x,v1._y-v2._y,v1._z-v2._z };
    }
    friend Vector3 operator * (const Vector3& v, const double& d)
    {
        return Vector3{v._x * d,v._y * d,v._z * d };
    }
    friend Vector3 operator / (const Vector3& v, const double& d)
    {
        return Vector3{v._x / d,v._y / d,v._z / d };
    }
    friend Vector3 operator += (Vector3& v1, const Vector3& v2)
    {
        return Vector3(v1._x = v1._x + v2._x, v1._y = v1._y + v2._y, v1._z = v1._z + v2._z);
    }
    friend ostream& operator << (ostream& os, const Vector3& v)
    {
        return os<<v._x<<';'<<v._y<<';'<<v._z<<';';
    }

public:
    Vector3()
        :_x{0},_y{0},_z{0} {}
    Vector3(double x, double y, double z)
        :_x{x},_y{y},_z{z} {}

    inline double sum() {return _x + _y + _z;}
    inline double square()  //return sum of square(^2)
    {
        return pow(_x,2.0) + pow(_y,2.0) + pow(_z,2.0);
    }
    inline double sqrt_2()  //return sqrt of square(^2) sum
    {
        return sqrt(square());
    }
};



#endif // VECTOR3_H
