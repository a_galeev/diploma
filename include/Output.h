#ifndef OUTPUT_H_INCLUDED
#define OUTPUT_H_INCLUDED
#include<iostream>
#include<vector>
#include<fstream>
#include"Vector3.h"
#include"Particle.h"
#include"Constants.h"

using namespace std;

/*ofstream log_file("log.dat"),
         vel_first_it("vel_first_it.dat"),
         vel_last_it("vel_last_it.dat"),
         init_vel("init_vel.dat"),
         init_int_en("init_int_en.dat"),
         int_en_first_iter("int_en_first_iter.dat"),
         int_en_last_iter("int_en_last_iter.dat"),
        temp_f("temp.dat");
        */


	//if(!temp_f.is_open()||!log_file.is_open()||!vel_first_it.is_open()||!vel_last_it.is_open()||
    //   !init_vel.is_open()||!init_int_en.is_open()||!int_en_first_iter.is_open()||!int_en_last_iter.is_open())
     //  throw runtime_error("Output file error");

void printInitVel(vector<Particle>& vp);
void printLastItVel(vector<Particle>& vp);
void printInitInt_En(vector<Particle>& vp);
void pritnLastItInt_En(vector<Particle>& vp);
void printCalcTemp(vector<Temperature>& vt);
void printNop(vector < vector < int > >& vnop);
void printK(vector < pair<double, double>>& vk);
void printAnalyticTemp(const double& _Z_r, const double& _Z_v);



#endif // OUTPUT_H_INCLUDED
