#ifndef RND_H_INCLUDED
#define RND_H_INCLUDED
#include<random>
#include<ctime>
using namespace std;


class Random
{
    const double epsilon = numeric_limits<double>::min();    //min number
    const double inf = numeric_limits<double>::max();    //max number

    time_t rd;   //random device
    mt19937 gen;

    uniform_real_distribution<double> std_distr_trunc;
    uniform_real_distribution<double> std_distr;

public:
    Random()
        :rd{time(NULL)}, gen{rd}, std_distr_trunc{epsilon,1}, std_distr{0,1} {}

    inline double getStdDistr() //get standard Distributed double value from [0,1]
    {
        return std_distr(gen);
    }

    inline double getStdDistr_trunc() //get standard Distributed double value from (0,1]
    {
        return std_distr_trunc(gen);
    }

    inline int getUniDistr(int a, int b)    //get Uniform Distributed int value from [a,b]
    {
        uniform_int_distribution<int> uid(a,b);
        return uid(gen);
    }
};

static Random Rand; //for random numbers

#endif // RND_H_INCLUDED
