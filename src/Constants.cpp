#include "Constants.h"
#include "Vector3.h"




map<string, Chem_el> initChemEl()
{
    static const string chem_file = "chem.db";
    ifstream ifs(chem_file);
    if(!ifs)
        throw runtime_error("Chemical data read error");
    else
    {
        map<string, Chem_el> result;
        string trash_title;
        getline(ifs,trash_title);
        string name;
        double mass,diameter, visc_indx,rotDof,vib_Char_Temp, diss_Char_temp;
        while( ifs>>name>>mass>>diameter>>visc_indx>>rotDof>>vib_Char_Temp>>diss_Char_temp)
        {
            result.insert(make_pair(name, Chem_el(mass,diameter,visc_indx,rotDof, vib_Char_Temp, diss_Char_temp)));
        }
        return result;
    }

}





