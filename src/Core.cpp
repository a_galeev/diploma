#include"Core.h"



inline double generateRotEn(const double& rot_dof,const double& rot_temp)
{
    double rnd;
    if (rot_dof == 0.0)
        return 0.0;
    if(rot_dof <= 2.0)
    {
        rnd = Rand.getStdDistr_trunc();
        return -log(rnd) * rot_temp *  kBoltzmann;
    }
    else
    {
        double en;
        do
        {
            rnd = Rand.getStdDistr();
            en = Rand.getStdDistr();
        }while(F_rot_norm(en) <= rnd);
        return en * rot_temp * kBoltzmann;
    }
}




inline double generateVibEn(const double& char_temp, const double& vib_temp)
{
    if(char_temp == 0.0)
        return 0.0;
    double rnd = Rand.getStdDistr_trunc();
    double n = floor(-log(rnd) * vib_temp / char_temp ); //number of vibrational levels (truncated)

    return kBoltzmann * char_temp * (n);  //summary vibrational energy
}








double generateVibEnAHO(const double& o_v, const double& o_d, const double& temp, const double& eta)
{
    int n;
    double r;
    do
    {
        n = Rand.getUniDistr(0, getNmaxAHO(o_v, o_d, eta));
        r = Rand.getStdDistr();

    }while(f_v_aho(o_v, o_d, temp, eta, n) <= r);

    return e_v_aho(o_v, o_d ,eta,n);

}


Vector3 generateVel(const double& mass, const double& t_tr, const Vector3& flow_vel)   //see Bird "Molecular Gas Dynamics", Appendix C
{
    double r1 = Rand.getStdDistr_trunc();
    double r2 = Rand.getStdDistr();
    double r3 = Rand.getStdDistr_trunc();
    double r4 = Rand.getStdDistr();
    double beta = calcBeta(mass, t_tr);

    double u_x = sqrt(-1.0 * log(r1)) * cos(2.0 * M_PI * r2) / beta;
    double u_y = sqrt(-1.0 * log(r1)) * sin(2.0 * M_PI * r2) / beta;
	double u_z = sqrt(-1.0 * log(r3)) * cos(2.0 * M_PI * r4) / beta;
	return Vector3(u_x, u_y, u_z) + flow_vel;

}

void init_particles(vector<Particle>& vp,const int id, const string element, const int numOfParts, const Temperature& temp, const Vector3& flow_vel)
{
    Vector3 vel;
    for(int i = 0;i<numOfParts;++i)
    {
        double mass = elements.at(element).getMass();
        double diameter = elements.at(element).getDiam();
        double visc_indx = elements.at(element).getViscInd();
        double rot_dof = elements.at(element).getRotDof();
        double vibChTemp = elements.at(element).getVibCharTemp();
        double dissChTemp = elements.at(element).getDissCharTemp();
        vel = generateVel(mass,temp.getT_tr(), flow_vel);
        double rotEn = generateRotEn(rot_dof,temp.getT_rot());
        double vibEn;
        if(temp.getT_vib() != 0)
            //vibEn = generateVibEn(vibChTemp, temp.getT_vib());
            vibEn = generateVibEnAHO(vibChTemp, dissChTemp, temp.getT_vib(), eta);
        else
            vibEn = 0.0;
        if(isnan(rotEn) || isnan(vibEn))
            cout<<"InitError";

        //double vibEn = kBoltzmann * vibChTemp * (1.0) * (1.0 - 0.82 * vibChTemp * (1.0) / (4.0 * dissChTemp) );
        //ouble vibEn = kBoltzmann * 3371.0 * numOfParts / (exp(3371.0 / 118.5) - 1.0);
        //double vibEn = generateVibEnAHO(vibChTemp, dissChTemp, temp.getT_vib(), eta);

        vp.push_back(Particle(vel, id, mass,diameter,visc_indx,rot_dof,rotEn,vibChTemp,dissChTemp,vibEn));
    }



}

double new_e_int(const double& dof, const double& viscInd, const double& e_coll)
{
    double a, b, c, r, en;
    do
    {
        r = Rand.getStdDistr();
        en = Rand.getStdDistr();
        a = (Y_r(dof) + Y_c(viscInd) - 2.0) / (Y_r(dof) - 1.0) * en;
		b = (Y_r(dof) + Y_c(viscInd) - 2.0) / (Y_c(viscInd) - 1.0) * (1.0 - en);
		c = pow(a, Y_r(dof) - 1.0) * pow(b, Y_c(viscInd) - 1.0);
    }while(r >= c);
    return en * e_coll;
}
