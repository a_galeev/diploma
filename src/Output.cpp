#include"Output.h"
#include "Constants.h"

void printInitVel(vector<Particle>& vp)
{
    ofstream init_vel("init_vel.dat");
    for( Particle& p : vp)
        init_vel<<p.getVel()<<endl;
    init_vel.close();

}

void printLastItVel(vector<Particle>& vp)
{
    ofstream vel_last_it("vel_last_it.dat");
    for( Particle& p : vp)
        vel_last_it<<p.getVel()<<endl;
    vel_last_it.close();
}

void printInitInt_En(vector<Particle>& vp)
{
    ofstream init_int_en("init_int_en.dat");
    for( Particle& p : vp)
        init_int_en<<p.getRotEn()<<endl;
    init_int_en.close();
}

void pritnLastItInt_En(vector<Particle>& vp)
{
    ofstream int_en_last_iter("int_en_last_iter.dat");
     for( Particle& p : vp)
        int_en_last_iter<<p.getRotEn()<<endl;
    int_en_last_iter.close();

}

void printCalcTemp(vector<Temperature>& vt)
{
    ofstream temp_f("temp_calc.dat");
    for(auto i = 0; i < vt.size(); ++i)
    {
        temp_f<< i * delta_t <<';'<<vt[i]<<endl;
    }
    temp_f.close();
}

void printNop(vector < vector < int > >& vnop)
{
    ofstream nof_f("num_of_part.dat");
    for(int i = 0; i < vnop.size();++i)
    {
        nof_f<<vnop[i][0]<<';'<<vnop[i][1]<<endl;
    }


}

void printK(vector < pair<double, double>>& vk)
{
    ofstream vk_f("arhenius_K.dat");

    for(pair<double, double>& el: vk)
    {
        vk_f<<el.first<<';'<<el.second<<endl;
    }


}


void printAnalyticTemp(const double& _Z_r, const double& _Z_v)
{

    ofstream temp_analyt("temp_analyt.dat");
    double t_tr, t_rot, t_vib;

    for (double i = 0; i < 100; i+=0.025)
	{
		t_tr = 300.0 + 400.0*exp(-i * 0.1798 * 2.0 / (_Z_r )); // 3/ 5* 10 * 2
		t_rot = 300 * (1 - exp(-i  * 0.1798 * 2.0/ (_Z_r ) ));
		t_vib = 300 * (1 - exp(-i  * 0.5/ (_Z_v ) ));
		temp_analyt << i << ';'<<t_tr <<';'<< t_rot <<';'<<t_vib<< endl;
	}
}


