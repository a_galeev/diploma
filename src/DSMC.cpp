#include"DSMC.h"


void DSMC::init_sigma_cr_arr (vector<string>& elem, const double& temp_tr)
{
    double m1,m2,d1,d2;
    sigma_cr_arr.resize(sz);
    w_coll_calc.resize(sz);
    for(int i = 0;i < sz;++i)
    {
        sigma_cr_arr[i].resize(sz);
        w_coll_calc[i].resize(sz);
        for(int j = i;j < sz;++j)
        {
            w_coll_calc[i][j] = 0;

            m1 = elements.at(elem[i]).getMass();
            m2 = elements.at(elem[j]).getMass();

            d1 = elements.at(elem[i]).getDiam();
            d2 = elements.at(elem[j]).getDiam();

            sigma_cr_arr[i][j] = generateSigmaCr(massReduced(m1,m2), valueAvg(d1,d2),temp_tr);
        }
    }

}

void DSMC::init_mf_arr(vector<int> &num_of_part )
{
    mf =0.0;
    mf_arr.resize(sz);
    nc_arr.resize(sz);

    for(int i = 0;i < sz; ++i)
    {
        mf_arr[i].resize(sz);
        nc_arr[i].resize(sz);

        for(int j = i; j < sz; ++j)
        {
            nc_arr[i][j] = 0;   //init collisions array by 0

            mf_arr[i][j] = delta2(i, j)* num_of_part[i] * (num_of_part[j] - delta1(i, j)) * sigma_cr_arr[i][j] * f_num / v_c;
            mf += mf_arr[i][j];
        }
    }
}

void DSMC::init_sp_sel()
{
    for(unsigned int i = 0; i < mf_arr.size(); ++i)
        for(unsigned int j = i; j < mf_arr.size(); ++j)
        {
            sp_w.push_back(mf_arr[i][j] / mf);
            pair_w.push_back(make_pair(i, j));
        }
    double sum_tmp = 0;
    sum_w.push_back(0.0);
    for(double& el : sp_w)
    {
        sum_tmp+=el;
        sum_w.push_back(sum_tmp);
    }
    sum_w[sum_w.size() - 1] = 1.0;
}

inline int DSMC::getRndParticle(vector<int> &num_of_part, int& id)
{
    if(id==0)//specie with the minimum id number
    {
        return Rand.getUniDistr(0,num_of_part[id] - 1);
    }
    else
    {

        int limit = 0;
        for(int it = 0;it <= id; it++)
        {


            limit+=num_of_part[it];

        }
        return Rand.getUniDistr(num_of_part[id-1], limit - 1);
    }
}

inline void DSMC::updateSigmaCr(const double& _sigma_cr)
{
    if(_sigma_cr > sigma_cr_arr[sp1][sp2])
    {
        sigma_cr_arr[sp1][sp2] = _sigma_cr;
        sigma_cr_arr[sp2][sp1] = _sigma_cr;
    }
}

inline void DSMC::collision(Particle& _p1, const int _pos1, Particle& _p2, const int _pos2, vector<Particle>& vec_p,  vector<int>& vec_nop, Temperature& temp)
{
    Vector3 cm = ((_p1.getVel() * _p1.getMass() ) +  (_p2.getVel() * _p2.getMass() )) / (_p1.getMass() + _p2.getMass());    //center of mass velocity
    double e_coll = massRed * cr_vec.square() / 2.0;    //energy of collision, on the first step initialized only by kinetic energy of particles
    double t_x = 59370;
    double t_y = 113400;
    Particle selP = _p2;
    Particle othP = _p1;
    int pos = _pos2;

    if((_p1.getDissTemp() == t_x && _p2.getDissTemp() == t_y))
    {
        selP = _p1;
        othP = _p2;
        pos = _pos1;
    }

    //double total_e_coll = e_coll + _p1.getRotEn() + _p1.getVibEn() + _p2.getRotEn() + _p2.getVibEn();
    double total_e_coll = e_coll + selP.getRotEn() + selP.getVibEn();

    double e_disss = kBoltzmann * selP.getDissTemp();
    double d0 = total_e_coll - e_disss;
    double diss_prob, diss_prob_bias;
    //double asd = _p1.getCharTemp();
    double sad = selP.getVibEn() / e_disss - 1.0;
    if(( (_p1.getDissTemp() == t_x  && _p2.getDissTemp() == t_y ) || (_p2.getDissTemp() == t_x  && _p1.getDissTemp() == t_y ))   && (d0 >= 0.0)  )
    {
        ch_c++;
        /*double intDof = _p1.getRotDof() + _p2.getRotDof() +  Xi_v(_p1.getCharTemp(), temp.getT_tr()) + Xi_v(_p2.getCharTemp(), temp.getT_tr());
        double fullDof = Xi_t(viscIndAvr) + intDof;
        double c1 = delta3(_p1.getId(), _p2.getId()) * Ad  * sqrt(massRed * M_PI / 8.0 / kBoltzmann);
        double c2 = (M_PI * pow(diamAvr, 2.0) * pow(kBoltzmann, Bd + viscIndAvr - 1.0) * pow(T_ref, viscIndAvr - 0.5));
        double c3 = tgamma(fullDof / 2.0);
        double c4 = tgamma(intDof / 2.0 + 1.5 + Bd);
        double c = c1 / c2 * c3 / c4;

        double d1 = pow(d0, intDof / 2.0 + 0.5 + Bd);
        double d2 = pow(total_e_coll, intDof / 2.0 + 1.5 - viscIndAvr);

        diss_prob = c *  d1 / d2;*/

        diss_prob = Ad_b * (1.0 - (e_disss - selP.getVibEn()) / (e_coll + selP.getRotEn())) * exp(Bd_b * (selP.getVibEn() / e_disss - 1.0));

    }
    else
        diss_prob = -1.0;

    //if(diss_prob != diss_prob)
    //    cout<<"ahtung";
    //if((diss_prob != -1.0 && diss_prob < 0.0) /*|| diss_prob > 1.0*/)  //for debug test
    //    cout<<"ERRORRR";
    //if(diss_prob >= 1.0)
    //    cout<<"Warning: Probability is more than unity! "<<diss_prob<<endl;

    rnd = Rand.getStdDistr();
    if(diss_prob >= rnd)
    {

        dissCounter++;
        /*total_e_coll -= e_disss;
        if(total_e_coll < 0)
           cout<<"zer";



        double rnd1, e_t_n, e_i;

        do
        {
            rnd1 = Rand.getStdDistr();
            e_t_n = Rand.getStdDistr();
        }while(rnd1 >= pow(e_t_n, 1.5 - viscIndAvr));
        e_t_n *= total_e_coll;  //new translational energy
        e_i = total_e_coll - e_t_n; //internal
        double r1 = Rand.getStdDistr();
        double r2 = Rand.getStdDistr();
        cr = sqrt(2.0 * total_e_coll / massRed);
        if(cr!=cr)
                cout<<"error1";
        double theta = acos(2.0 * r1 - 1.0);
        double phi = 2.0 * M_PI * r2;
        Vector3 rotation(cos(theta), sin(theta) * cos(phi), sin(theta) * sin(phi));
        cr_vec = rotation * cr;
        //��������� ����������� �� ����� otherP, ������ ����� ���
        othP.setVel(cm - (cr_vec * (selP.getMass() / (_p1.getMass() + _p2.getMass()))));   //distribution relative velocity value between particles components
        Vector3 selP_vel_n = (cm + (cr_vec * (othP.getMass() / (_p1.getMass() + _p2.getMass()))));

        Particle part1 = othP;  //two parts of dissociated oxygen molecule
        Particle part2 = othP;

        cm = selP_vel_n;
        cr = 2.0 * sqrt(e_i / othP.getMass());
        if(cr!=cr)
                cout<<"error2";

        r1 = Rand.getStdDistr();
        r2 = Rand.getStdDistr();

        theta = acos(2.0 * r1 - 1.0);
        phi = 2.0 * M_PI * r2;
        Vector3 rotation2(cos(theta), sin(theta) * cos(phi), sin(theta) * sin(phi));
        cr_vec = rotation2 * cr;

        part1.setVel(cm + (cr_vec * (part2.getMass() / (part1.getMass() + part2.getMass()))));   //distribution relative velocity value between particles components
        part2.setVel(cm - (cr_vec * (part1.getMass() / (part1.getMass() + part2.getMass()))));


        vec_nop[selP.getId()] -= 1;
        vec_nop[othP.getId()] += 2;
        vec_p.erase(vec_p.begin() + pos);
        vec_p.push_back(part1);
        vec_p.push_back(part2);
        */

        //for debugging
        //---------------------------------------------------------------------------------
        double rndqw = Rand.getStdDistr();
        double pr1 = 1.0 / Z_r(_p1.getRotDof(), _p1.getViscInd(),temp.getT_tr() );  //probability of inelastic rotational collision of first particle
        double pr2 = pr1 + 1.0 / Z_r(_p2.getRotDof(), _p2.getViscInd() ,temp.getT_tr());   //probability of inelastic rotational collision of second particle
        double pr3 = pr2 + 1.0 / Z_v(_p1.getCharTemp(),_p1.getViscInd(),temp.getT_tr(), massRed, diamAvr);
        double pr4 = pr3 + 1.0 / Z_v(_p2.getCharTemp(),_p2.getViscInd(),temp.getT_tr(), massRed, diamAvr);
        double e_t_new;
        if( pr1 >= rndqw && _p1.getCharTemp() > 0)   //inelastic translational - rotational collision of first particle
        {
            inelastic++;
            e_coll += _p1.getRotEn();   //plus rotational energy of particle
            double e_int_new = new_e_int(_p1.getRotDof(), _p1.getViscInd(), e_coll);    //internal energy after collision
            e_t_new = e_coll - e_int_new;    //translational energy after collision
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            _p1.setRotEn(e_int_new);    //set new particle rotational energy
            if(cr!=cr)
                cout<<"error";
        }
        else if(pr2 >= rndqw && _p2.getCharTemp() > 0) //inelastic translational - rotational collision of second particle
        {
            inelastic++;
            e_coll += _p2.getRotEn();   //plus rotational energy of particle
            double e_int_new = new_e_int(_p2.getRotDof(), _p2.getViscInd(), e_coll);    //internal energy after collision
            e_t_new = e_coll - e_int_new;    //translational energy after collision
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            _p2.setRotEn(e_int_new);    //set new particle rotational energy
            if(cr!=cr)
                cout<<"error";

        }
        else if(pr3 >= rndqw && _p1.getCharTemp() > 0) //inelastic tr-vib collision of first particle
        {
            inelastic++;
            e_coll += _p1.getVibEn();
            double e_diss = kBoltzmann * _p1.getDissTemp();
            //double n_max = trunc(e_coll / kBoltzmann / _p1.getCharTemp());
            double n_max = int(2.0 * _p1.getDissTemp() / _p1.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta);



            //double n_max1 = 2.0 * _p1.getDissTemp() / _p1.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta;
            //double n_max = trunc(n_max1 - eta );

            double n, e_v, rnd1, rnd2;
            do
            {

                //rnd1 = Rand.getStdDistr();
                rnd2 = Rand.getStdDistr();
                n = Rand.getUniDistr(0,n_max);
                //e_v = kBoltzmann * _p1.getCharTemp() * (n);   //SHO model
                e_v = e_v_aho(_p1.getCharTemp(), _p1.getDissTemp(),eta,n);  //AHO model
            }while(rnd2 >= F_vib_norm(e_v / e_coll, _p1.getViscInd())); //acceptence-rejection method
            e_t_new = e_coll - e_v;
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            _p1.setVibEn(e_v);
            if(cr!=cr)
                cout<<"error";
        }
        else if(pr4 >= rndqw && _p2.getCharTemp() > 0)
        {
            inelastic++;
            e_coll += _p2.getVibEn();
            double e_diss = kBoltzmann * _p2.getDissTemp();
            //double n_max = trunc(e_coll / kBoltzmann / _p2.getCharTemp());
            double n_max = int(2.0 * _p2.getDissTemp() / _p2.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta);


            //double n_max1 = 2.0 * _p2.getDissTemp() / _p2.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta;

            //double n_max = trunc(n_max1 - eta);

            double n, e_v, rnd1, rnd2;
            do
            {
               // rnd1 = Rand.getStdDistr();
                rnd2 = Rand.getStdDistr();
                n = Rand.getUniDistr(0,n_max);
                //e_v = kBoltzmann * _p2.getCharTemp() * (n);
                e_v = e_v_aho(_p2.getCharTemp(), _p2.getDissTemp(),eta,n);
            }while(rnd2 >= F_vib_norm(e_v / e_coll,_p2.getViscInd()));
            e_t_new = e_coll - e_v;
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            if(cr!=cr)
                cout<<"error";
            _p2.setVibEn(e_v);
        }
        //else elastic collision
        double r1 = Rand.getStdDistr();
        double r2 = Rand.getStdDistr();

        double theta = acos(2.0 * r1 - 1.0);
        double phi = 2.0 * M_PI * r2;
        Vector3 rotation(cos(theta), sin(theta) * cos(phi), sin(theta) * sin(phi));
        cr_vec = rotation * cr;

        _p1.setVel(cm + (cr_vec * (_p2.getMass() / (_p1.getMass() + _p2.getMass()))));   //distribution relative velocity value between particles components
        _p2.setVel(cm - (cr_vec * (_p1.getMass() / (_p1.getMass() + _p2.getMass()))));

        //__________________________________________________________________________________________________________________________


    }
    else
    {
        double rndqw = Rand.getStdDistr();
        double pr1 = 1.0 / Z_r(_p1.getRotDof(), _p1.getViscInd(),temp.getT_tr() );  //probability of inelastic rotational collision of first particle
        double pr2 = pr1 + 1.0 / Z_r(_p2.getRotDof(), _p2.getViscInd() ,temp.getT_tr());   //probability of inelastic rotational collision of second particle
        double pr3 = pr2 + 1.0 / Z_v(_p1.getCharTemp(),_p1.getViscInd(),temp.getT_tr(), massRed, diamAvr);
        double pr4 = pr3 + 1.0 / Z_v(_p2.getCharTemp(),_p2.getViscInd(),temp.getT_tr(), massRed, diamAvr);
        double e_t_new;
        if( pr1 >= rndqw && _p1.getCharTemp() > 0)   //inelastic translational - rotational collision of first particle
        {
            inelastic++;
            e_coll += _p1.getRotEn();   //plus rotational energy of particle
            double e_int_new = new_e_int(_p1.getRotDof(), _p1.getViscInd(), e_coll);    //internal energy after collision
            e_t_new = e_coll - e_int_new;    //translational energy after collision
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            _p1.setRotEn(e_int_new);    //set new particle rotational energy
            if(cr!=cr)
                cout<<"error";
        }
        else if(pr2 >= rndqw && _p2.getCharTemp() > 0) //inelastic translational - rotational collision of second particle
        {
            inelastic++;
            e_coll += _p2.getRotEn();   //plus rotational energy of particle
            double e_int_new = new_e_int(_p2.getRotDof(), _p2.getViscInd(), e_coll);    //internal energy after collision
            e_t_new = e_coll - e_int_new;    //translational energy after collision
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            _p2.setRotEn(e_int_new);    //set new particle rotational energy
            if(cr!=cr)
                cout<<"error";

        }
        else if(pr3 >= rndqw && _p1.getCharTemp() > 0) //inelastic tr-vib collision of first particle
        {
            inelastic++;
            e_coll += _p1.getVibEn();
            double e_diss = kBoltzmann * _p1.getDissTemp();
            //double n_max = trunc(e_coll / kBoltzmann / _p1.getCharTemp());
            double n_max = int(2.0 * _p1.getDissTemp() / _p1.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta);



            //double n_max1 = 2.0 * _p1.getDissTemp() / _p1.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta;
            //double n_max = trunc(n_max1 - eta );

            double n, e_v, rnd1, rnd2;
            do
            {

                //rnd1 = Rand.getStdDistr();
                rnd2 = Rand.getStdDistr();
                n = Rand.getUniDistr(0,n_max);
                //e_v = kBoltzmann * _p1.getCharTemp() * (n);   //SHO model
                e_v = e_v_aho(_p1.getCharTemp(), _p1.getDissTemp(),eta,n);  //AHO model
            }while(rnd2 >= F_vib_norm(e_v / e_coll, _p1.getViscInd())); //acceptence-rejection method
            e_t_new = e_coll - e_v;
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            _p1.setVibEn(e_v);
            if(cr!=cr)
                cout<<"error";
        }
        else if(pr4 >= rndqw && _p2.getCharTemp() > 0)
        {
            inelastic++;
            e_coll += _p2.getVibEn();
            double e_diss = kBoltzmann * _p2.getDissTemp();
            //double n_max = trunc(e_coll / kBoltzmann / _p2.getCharTemp());
            double n_max = int(2.0 * _p2.getDissTemp() / _p2.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta);


            //double n_max1 = 2.0 * _p2.getDissTemp() / _p2.getCharTemp() * (1.0 - sqrt(1.0 - eta * min(e_coll, e_diss) / e_diss)) / eta;

            //double n_max = trunc(n_max1 - eta);

            double n, e_v, rnd1, rnd2;
            do
            {
               // rnd1 = Rand.getStdDistr();
                rnd2 = Rand.getStdDistr();
                n = Rand.getUniDistr(0,n_max);
                //e_v = kBoltzmann * _p2.getCharTemp() * (n);
                e_v = e_v_aho(_p2.getCharTemp(), _p2.getDissTemp(),eta,n);
            }while(rnd2 >= F_vib_norm(e_v / e_coll,_p2.getViscInd()));
            e_t_new = e_coll - e_v;
            cr = sqrt(2.0 * e_t_new / massRed); //relative velocity magnitude after collision
            if(cr!=cr)
                cout<<"error";
            _p2.setVibEn(e_v);
        }
        //else elastic collision
        double r1 = Rand.getStdDistr();
        double r2 = Rand.getStdDistr();

        double theta = acos(2.0 * r1 - 1.0);
        double phi = 2.0 * M_PI * r2;
        Vector3 rotation(cos(theta), sin(theta) * cos(phi), sin(theta) * sin(phi));
        cr_vec = rotation * cr;

        _p1.setVel(cm + (cr_vec * (_p2.getMass() / (_p1.getMass() + _p2.getMass()))));   //distribution relative velocity value between particles components
        _p2.setVel(cm - (cr_vec * (_p1.getMass() / (_p1.getMass() + _p2.getMass()))));

    }


}




Temperature DSMC::calcTemp(vector<Particle>& _p, vector<int> &num_of_part)
{
    vector<Vector3> sum_vel_2(sz);
    vector<Vector3>sum_vel(sz);
    vector<Vector3> temp_tr_s(sz);
    vector<double> temp_rot_s(sz);
    vector<double> temp_vib_s(sz);
    vector<double> e_rot_s(sz);
    vector<double> e_vib_s(sz);
    vector<double> mass(sz);
    vector<double> dof(sz);
    vector<double> v_dof(sz);
    vector<double> chTemp(sz);
    Vector3 temp_tr;    //translational temperature
    double temp_rot_a = 0, temp_rot_b = 0, alpha, temp_ov_sp;
    double temp_vib_a = 0, temp_vib_b = 0;
    double temp_ov_a = 0, temp_ov_b = 0;
    int min_id = 10;
    for(Particle& el : _p)
    {
        sum_vel_2[el.getId()] += el.getVel() * el.getVel();
        sum_vel[el.getId()] += el.getVel() / num_of_part[el.getId()];
        e_rot_s[el.getId()] += el.getRotEn();
        e_vib_s[el.getId()] += el.getVibEn();
        if( mass[el.getId()] == 0 )
            mass[el.getId()] = el.getMass();
        if(dof[el.getId()] == 0)
            dof[el.getId()] = el.getRotDof();
        if(chTemp[el.getId()] == 0)
            chTemp[el.getId()] = el.getCharTemp();
        if (el.getId() < min_id)
            min_id = el.getId();
    }


    for(int i = min_id; i < sz; ++i)
    {

        temp_tr_s[i] += (sum_vel_2[i] - sum_vel[i] * sum_vel[i]) * mass[i] / kBoltzmann / num_of_part[i];
        alpha = double(num_of_part[i]) / _p.size();

        temp_tr += temp_tr_s[i] * alpha;

        //cout<<"temp_tr_s[i] = "<< temp_tr_s[i]<<endl;
        //cout<<"temp_tr = "<< temp_tr<<endl;

        if(dof[i] == 0)
            temp_rot_s[i] = 0;
            //temp_rot += 0;
        else
        {
            temp_rot_s[i] = 2.0 / dof[i] / kBoltzmann * e_rot_s[i] / num_of_part[i];
            temp_rot_a += (alpha * dof[i] * temp_rot_s[i]);
            temp_rot_b += (alpha * dof[i]);
        }
        //cout<<"temp_rot_s[i] = "<<temp_rot_s[i]<<endl;
        //cout<<"temp_rot = "<<temp_rot<<endl;

        if(chTemp[i] == 0 || e_vib_s[i] == 0)
        {
            temp_vib_s[i] = 0;
            v_dof[i] = 0;
        }
        else
        {
            temp_vib_s[i] = chTemp[i] / log(kBoltzmann * chTemp[i] * num_of_part[i] / e_vib_s[i] + 1.0);

            v_dof[i] = 2.0 * chTemp[i] / temp_vib_s[i] / (exp(chTemp[i] / temp_vib_s[i]) - 1.0);
            temp_vib_a += (alpha * v_dof[i] * temp_vib_s[i]);
            temp_vib_b += (alpha * v_dof[i]);

        }

        //cout<<"v_dof[i] = "<<v_dof[i]<<endl;
        //cout<<"temp_vib_s[i] = "<< temp_vib_s[i]<<endl;
        //cout<<"temp_vib = "<< temp_vib_a<<" and "<<temp_vib_b <<endl;


        temp_ov_sp = (temp_tr_s[i].sum() + dof[i] * temp_rot_s[i] + v_dof[i] * temp_vib_s[i]) / (3.0 + dof[i] + v_dof[i]);
        temp_ov_a += (alpha * temp_ov_sp * (3.0 + dof[i] + v_dof[i])) ;
        temp_ov_b += (alpha * (3.0 + dof[i] + v_dof[i]));

        //cout<<"temp_ov_sp = "<<temp_ov_sp<<endl;
        //cout<<"temp_ov = "<<temp_ov<<endl;

    }
    double t_r = (temp_rot_b == 0) ? 0 : temp_rot_a / temp_rot_b;
    double t_v = (temp_vib_b == 0) ? 0 : temp_vib_a / temp_vib_b;

    return Temperature(temp_tr.sum() / 3.0, t_r, t_v, temp_ov_a / temp_ov_b);
    //return Temperature(temp_tr.sum() / 3.0, temp_rot, temp_vib, temp_ov);
    //return Temperature(temp_tr.sum() / 3.0, temp_rot_s[0], temp_vib_s[0], temp_ov_sp);

   // _temp.setTemp(temp_tr.sum() / 3.0, temp_rot, temp_ov);

}

void DSMC::init_Wcoll_theory(vector<string>& elem, vector<int> &num_of_part, double temp)
{
    w_coll_theory.resize(sz);
    double d1,d2, m1,m2, vI1, vI2, n1, n2;
    for(int i = 0;i<sz;++i)
    {

        w_coll_theory[i].resize(sz);
        for(int j = i;j<sz;++j)
        {


            d1 = elements.at(elem[i]).getDiam();
            d2 = elements.at(elem[j]).getDiam();

            m1 = elements.at(elem[i]).getMass();
            m2 = elements.at(elem[j]).getMass();

            vI1 = elements.at(elem[i]).getViscInd();
            vI2 = elements.at(elem[j]).getViscInd();

            n1 = num_of_part[i] * f_num / v_c;
            n2 = num_of_part[j] * f_num / v_c;

            w_coll_theory[i][j] = delta4(i,j)* pow(valueAvg(d1,d2), 2.0) * n1 * (n2 - delta1(i, j))  * sqrt( 2.0 * M_PI * kBoltzmann * T_ref / massReduced(m1,m2)) * pow(temp / T_ref, 1.0 - valueAvg(vI1, vI2));
        }
    }
}

DSMC::DSMC(const int& num_of_iterations, vector<string>& elem, vector<int> &num_of_part, Temperature& temp)
{
    vector<Particle> vp;
    vector<Temperature> vt;
    vector<pair<double, double>> vk;
    vector < vector < int > > vnop;
    vnop.push_back(num_of_part);
    sz = elem.size();
    double step = 0;
    for( int it = 0; it<sz;++it)
        init_particles(vp, it, elem[it], num_of_part[it], temp, Vector3(0,0,0));

    printInitVel(vp);
    printInitInt_En(vp);
    // In current state, only the Majorant Frequency Scheme is used

    temp = calcTemp(vp,num_of_part);  //calibration after initialization
    //cout<<"Init temp = "<<temp<<endl;
    vt.push_back(temp);
    double k_theor = 0.0;
    double k_calc = 0.0;
    double tavr = 0;
    double tdddd = vp[0].getDissTemp();
    double total_part_num = vp.size();
    f_num = number_dens / total_part_num;
    init_Wcoll_theory(elem, num_of_part, temp.getT_ov());
    double dTau, tau, dt;   //different time step variables
    double prob;    //probability of species collision
    double w_comp_avr;
    init_sigma_cr_arr(elem, temp.getT_tr());
    for(int iter = 0;iter < num_of_iterations;++iter)
    {
        sz = elem.size();
        init_mf_arr(num_of_part);
        init_sp_sel();

        dTau = generate_dTau(mf);    //time step (delta Tau) as a function of total majorant collision frequency
        tau = dTau;  //local cell time?
        int qwe = 0;
        while(tau < delta_t)    //main MFS cycle
        {

            rnd = Rand.getStdDistr();

            for(unsigned int i = 0; i < sum_w.size() - 1;++i)
                if ((sum_w[i] <= rnd) && (sum_w[i + 1] > rnd))  //select species according to their selection probabilities
                {
						sp1 = pair_w[i].first;
						sp2 = pair_w[i].second;
                }

            int n1, n2;
            do
            {
                n1 = getRndParticle(num_of_part, sp1);
                n2 = getRndParticle(num_of_part, sp2);
            }while(n1 == n2); //choose random particle numbers with selected species and check that they're different

            Particle& p1 = vp[n1];  //choose particles according to proposed numbers
            Particle& p2 = vp[n2];

            cr_vec = p1.getVel() - p2.getVel(); //vector (x,y,z) of relative velocity
            cr = cr_vec.sqrt_2();   //absolute value (magnitude) of relative velocity
            massRed = massReduced(p1.getMass(), p2.getMass());  //reduced mass
            diamAvr = valueAvg(p1.getDiam(), p2.getDiam()); //average diameter of two particles
            viscIndAvr = valueAvg(p1.getViscInd(), p2.getViscInd());    //average viscosity index

            //rotDofAvr = valueAvg(p1.getRotDof(), p2.getRotDof());
            sigma_cr = calcSigmaVHS(T_ref, diamAvr, massRed, cr, viscIndAvr) * cr;  //calculate collision cross-section * relative velocity
            if(sigma_cr > sigma_cr_arr[sp1][sp2])
                updateSigmaCr(sigma_cr);
            prob = sigma_cr / sigma_cr_arr[sp1][sp2];

            rnd = Rand.getStdDistr();
            if(prob >= rnd)
            {
                nc++;
                nc_arr[sp1][sp2]++;
                //if(sp1!=sp2)
                //    nc_arr[sp2][sp1]++;

                collision(p1, n1, p2, n2, vp, num_of_part, temp);
                //vp[n1] = p1;
                //vp[n2] = p2;
                //if(nc % 1000 == 0)
                //    temp = calcTemp(vp,num_of_part);
                    //calibrateTrTemp(vp,num_of_part,temp);
            }

            qwe++;
            //if((qwe % 500) == 0)
            //    temp = calcTemp(vp,num_of_part);

            //sp_w.clear();
            //pair_w.clear();
            //sum_w.clear();
            //init_mf_arr(num_of_part);
            //init_sp_sel();
            dt = generate_dTau(mf); //increase timestep
            tau+=dt;


        }


        temp = calcTemp(vp,num_of_part);

        sp_w.clear();
        pair_w.clear();
        sum_w.clear();


        cout<<iter+1<<')'<<" Number of collisions = " << nc << " nc / nm = " << double(nc) / vp.size() <<endl;

        cout<<temp<<endl;
        //k_theor = Ad * pow(temp.getT_ov(), Bd) * exp(- 113400.0 / temp.getT_ov());       //59370
        //cout<<"k_analytic: "<<k_theor <<endl;

        //k_calc += dissCounter * v_c / (f_num * num_of_part[0] * (num_of_part[1])  * delta_t);
        //cout<<"k_dsmc: "<<k_calc / (iter + 1) <<endl<<endl;


        tavr += temp.getT_ov();

        cout <<"Temp_avr: "<<tavr / (iter + 1)<<endl;

        //cout<<num_of_part[0]<<' '<<num_of_part[1]<<endl;


        //init_Wcoll_theory(elem, num_of_part, temp.getT_ov());
        //for(unsigned int i = 0; i < sz;++i)
        //    for(unsigned int j = i; j < sz;++j)
        //    {
        //        w_coll_calc[i][j] += nc_arr[i][j] * f_num / v_c / delta_t;
        //    }

        //for(unsigned int i = 0; i < sz;++i)
        //    for(unsigned int j = i; j < sz;++j)
        //    {
        //        cout<<w_coll_theory[i][j]<<endl;
        //        cout<<w_coll_calc[i][j] / (iter+1)<<endl;

        //    }
        vt.push_back(temp);
        vnop.push_back(num_of_part);
        //vk.push_back(make_pair(k_theor, k_calc / (iter + 1)));*/



        nc = 0;
        //dissCounter = 0;
        ch_c = 0;


    }
    //k_theor = Ad * pow(temp.getT_ov(), Bd) * exp(- 113400.0 / temp.getT_ov());       //59370
    //cout<<"k_analytic: "<<k_theor <<endl;

    //k_calc = dissCounter * v_c / (f_num * num_of_part[0] * (num_of_part[1])  * delta_t);
    //cout<<"k_dsmc: "<<k_calc / (num_of_iterations) <<endl;
    //cout <<"Temp_avr: "<<tavr / (num_of_iterations)<<endl;
    //cout<<"------------------------------------------------------------"<<endl;
    //cout<<tavr / (num_of_iterations)<<'\t'<<k_calc / (num_of_iterations)<<endl;
    printLastItVel(vp);
    pritnLastItInt_En(vp);


    printCalcTemp(vt);
    printNop(vnop);
    printK(vk);



}
