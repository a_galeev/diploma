#include <exception>
#include<iostream>
#include<vector>
#include"DSMC.h"
#include"Constants.h"


using namespace std;

int main()
{
    try
    {

        const int its = 1000;
        vector<string> elems{"O2", "N2"};
        vector<int> nop{80000, 20000};

        //double ev = kBoltzmann * 3371.0 * 100000 / (exp(3371.0 / 200.0) - 1.0);
        cout<<"Specie: ";
        for(string& el : elems)
            cout<<el;
        cout<<endl;
        /*for(double t = 8000.0;t <= 20000.0; t+= 2000.0)
        {
            Temperature temp(t, t, t, 0.0);
            DSMC start(its,elems,nop,temp);
        }*/
        Temperature temp(20000.0, 20000.0, 20000.0, 0.0);
        DSMC start(its, elems, nop, temp);












    }
    catch(runtime_error& e)
    {
        cerr<<e.what()<<endl;
    }
    system("pause");
    return 0;
}
